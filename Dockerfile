FROM theopencompany/alpine-winter-base
ARG WINTER_VERSION=master

RUN git clone -b $WINTER_VERSION https://samuaz@bitbucket.org/theopencompany/winter.git && \
cd winter && \
mkdir build && \
cd build && \
cmake .. -DALPINE_BUILD=TRUE && \
sudo make install

